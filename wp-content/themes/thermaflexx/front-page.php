<?php get_header();?>

<div class="area" id="carousel">
  <ul class="home_carousel">
    <li class="item1">
      <img src="<?php the_field('bg_image'); ?>" alt=""/>
      <div class="overlay">
        <h1><?php the_field('main_slogan'); ?><span><?php the_field('sub_slogan'); ?></span></h1>
      </div>
    </li>
  </ul>
  <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_wi-trans.png" class="divider top">
  <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_red-zw.png" class="divider bottom">
</div>


<div class="area" id="submenu">
  <div class="grid">
    <div class="row">
      <div class="col120">
        <ul class="clearfix">
            <?php if( have_rows('plate_info') ): 
                while ( have_rows('plate_info') ) : the_row();?>
                <li>
                    <a href="<?php the_sub_field('plate_link');?>">
                        <h2>
                          <span data-hover="<?php the_sub_field('plate_size');?>"><?php the_sub_field('plate_size');?></span>
                        </h2>
                        <p>max plate size</p>
                    </a>
                </li>
            <?php 
                endwhile; 
            endif;?>
        </ul>
      </div>
    </div>
  </div>
</div>

<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_zw-wi.jpg" class="divider">

<div class="area" id="intro">
  <div class="grid">
    <div class="row">
      <div class="col14">
        <?php the_field('home_description'); ?>
      </div>
      <div class="col5 push1">
        <div class="vertical-line">
          <div class="text-list">
            <?php the_field('home_benefits'); ?>
            <a href="/your-benefits" class="more">Read more</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="area" id="highlights">
  <div class="grid">
    <div class="row">
      <div class="col20">

        <div class="block-list">
          <h2>What you shouldn't miss</h2>

          <ul class="clearfix main-news">     
            <?php
                $args = array(
                  'order' => 'DESC',
                  'posts_per_page'    =>  -1,
                );
                $query = new WP_Query( $args );
            ?>
            <?php if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post(); ?>  
              <?php if( get_field('show_on_homepage') ){?>     
                <li>
                  <a href="<?php the_permalink();?>">
                    <div class="photo">
                      <div class="inner"></div>
                      <?php the_post_thumbnail();?>
                    </div>
                    <h4><?php the_title();?></h4>
                    <p><?php the_excerpt();?></p>
                  </a>
                </li>
              <?php }?>
            <?php endwhile;?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
          </ul>

        </div>
      </div>
    </div>
  </div>
</div>


<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_wi-gr.jpg" class="divider" style="background: #f1f1f1;">


<div class="area" id="news">
  <div class="grid">
    <div class="row">
      <div class="col5">

        <h2>ThermoFlexX <strong>news</strong></h2>
        <div class="news-list">
            <div class="bx-wrapper">
                <div class="bx-viewport">
                    <ul class="slider">
                        <?php
                            $args = array(
                              'posts_per_page'    =>  -1,
                            );
                            $query = new WP_Query( $args );
                        ?>
                        <?php if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post(); ?>
                        <li>
                            <a href="<?php the_permalink();?>" class="home-news">
                                <h4><?php the_title();?></h4>
                                Posted on <?php the_date('d F Y');?>         
                            </a>
                        </li>
                        <?php endwhile;?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>
                    </ul>
                </div>

            </div>
        </div>
      </div>
      <div class="col14 push1">
        <div class="vertical-line news-detail">
          <div class="content">
            <div class="date">
                <?php
                    $args = array(
                      'posts_per_page'    =>  1,
                    );
                    $query = new WP_Query( $args );
                ?>
                <?php if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post(); ?>
                <?php the_date('M');?> <span></span>
                <?php endwhile;?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>

            <div id="news-content">
                <?php
                    $args = array(
                      'posts_per_page'    =>  1,
                    );
                    $query = new WP_Query( $args );
                ?>
                <?php if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post(); ?>
                <h2><?php the_title();?></h2>
                <?php the_post_thumbnail( 'post-thumbnail', array( 'class'  => 'attachment-post-thumbnail size-post-thumbnail scale' ) );?>
                <p><?php the_excerpt();?></p>
                <a href="<?php the_permalink();?>" class="more">Read more</a>
                <?php endwhile;?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer();?>