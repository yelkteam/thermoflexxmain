<?php
function thermaflexx() {

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(array(
        'menu-top' =>__( 'Menu top')
    ) );

    // This theme uses a custom image size for featured images, displayed on "standard" posts.
    add_theme_support( 'post-thumbnails' );

}
add_action( 'after_setup_theme', 'thermaflexx' );

// connect styles & scripts
function styles(){
    if (!is_page_template('landing.php')) {
        wp_register_style( 'normalize', get_template_directory_uri() . '/assets/css/normalize.css', filemtime(get_template_directory() . '/assets/css/normalize.css'));
        wp_enqueue_style( 'normalize' );
        wp_register_style( 'bxslider', get_template_directory_uri() . '/assets/css/bxslider.css', filemtime(get_template_directory() . '/assets/css/bxslider.css'));
        wp_enqueue_style( 'bxslider' );
        wp_register_style( 'grid', get_template_directory_uri() . '/assets/css/grid.css', filemtime(get_template_directory() . '/assets/css/grid.css'));
        wp_enqueue_style( 'grid' );
        wp_register_style( 'meanmenu', get_template_directory_uri() . '/assets/css/meanmenu.css', filemtime(get_template_directory() . '/assets/css/meanmenu.css'));
        wp_enqueue_style( 'meanmenu' );
        wp_register_style( 'main-v2', get_template_directory_uri() . '/assets/css/main-v2.css', filemtime(get_template_directory() . '/assets/css/main-v2.css'));
        wp_enqueue_style( 'main-v2' );
    }
}
add_action( 'wp_enqueue_scripts', 'styles' );

function scripts(){
    if (!is_page_template('landing.php')) {
        wp_enqueue_script( 'jquery' );
        wp_register_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr-2.6.2.min.js', array(), '', filemtime(get_template_directory() . '/assets/js/modernizr-2.6.2.min.js'), true );
        wp_enqueue_script( 'modernizr' );

        wp_register_script( 'bxslider', get_template_directory_uri() . '/assets/js/jquery.bxslider.min.js', array(), '', filemtime(get_template_directory() . '/assets/js/jquery.bxslider.min.js'), true );
        wp_enqueue_script( 'bxslider' );

        wp_register_script( 'meanmenu', get_template_directory_uri() . '/assets/js/jquery.meanmenu.2.0.min.js', array(), '', filemtime(get_template_directory() . '/assets/js/jquery.meanmenu.2.0.min.js'), true );
        wp_enqueue_script( 'meanmenu' );

        wp_register_script( 'scrollTo', get_template_directory_uri() . '/assets/js/jquery.scrollTo.min.js', array(), '', filemtime(get_template_directory() . '/assets/js/jquery.scrollTo.min.js'), true );
        wp_enqueue_script( 'scrollTo' ); 

        wp_register_script( 'placeholders', get_template_directory_uri() . '/assets/js/placeholders.min.js', array(), '', filemtime(get_template_directory() . '/assets/js/placeholders.min.js'), true );
        wp_enqueue_script( 'placeholders' );
        
        wp_register_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array(), '', filemtime(get_template_directory() . '/assets/js/main.js'), true );
        wp_enqueue_script( 'main' );
    }
}
add_action( 'wp_enqueue_scripts', 'scripts' );

function land(){
    if (is_page_template('landing.php')) {
        wp_enqueue_script( 'jquery' );
        wp_register_style( 'land', get_template_directory_uri() . '/assets/css/landing/land.css', filemtime(get_template_directory() . '/assets/css/landing/land.css'));
        wp_enqueue_style( 'land' );

        wp_register_style( 'animate', get_template_directory_uri() . '/assets/css/landing/animate.css', filemtime(get_template_directory() . '/assets/css/landing/animate.css'));
        wp_enqueue_style( 'animate' );

        wp_register_script( 'land', get_template_directory_uri() . '/assets/js/landing/land.js', array(), '', filemtime(get_template_directory() . '/assets/js/landing/land.js'), true );
        wp_enqueue_script( 'land' );
        wp_register_script( 'wow', get_template_directory_uri() . '/assets/js/landing/wow.js', array(), '', filemtime(get_template_directory() . '/assets/js/landing/wow.js'), true );
        wp_enqueue_script( 'wow' );
    }
}
add_action( 'wp_enqueue_scripts', 'land' );

function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');

// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);