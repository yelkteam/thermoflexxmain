<!doctype html>
<html class="no-js" lang=""> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php bloginfo( 'title' ); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
        <link rel="shortcut icon" type="image/png" href="<?php echo get_stylesheet_directory_uri();?>/assets/img/landing/favicon.png"/>
        <?php wp_head();?>
    </head>
    <body>
        <div class="header-container">
            <header class="header">

                <div class="header_left">
                    <nav class="header_left_menu">
                        <a href="/landing" class="logo">
                            <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/landing/Logo.svg" alt="logo" />
                        </a>
                        <button class="bars">
                            <svg class="tcb-icon" viewBox="0 0 448 512" data-id="icon-bars-light" data-name="">
                                <path d="M442 114H6a6 6 0 0 1-6-6V84a6 6 0 0 1 6-6h436a6 6 0 0 1 6 6v24a6 6 0 0 1-6 6zm0 160H6a6 6 0 0 1-6-6v-24a6 6 0 0 1 6-6h436a6 6 0 0 1 6 6v24a6 6 0 0 1-6 6zm0 160H6a6 6 0 0 1-6-6v-24a6 6 0 0 1 6-6h436a6 6 0 0 1 6 6v24a6 6 0 0 1-6 6z"></path>
                            </svg>
                        </button>
                        <?php wp_nav_menu( [ 'menu' => 'Landing', 'container' => 'false' ] ); ?>
                    </nav>
                    <div class="header_left_title">
                        <h1 class="wow fadeInLeft"><?php the_field('title');?></h1>
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            <h2 class="wow fadeInUp"><?php the_content();?></h2>
                        <?php endwhile;?>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="header_right">
                    <?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'attachment-post-thumbnail size-post-thumbnail wow fadeInUp' ) );?>
                    <div class="header_right_rectangle"></div>
                </div>
                
            </header>
        </div>
        <div class="overlay">
            <div class="modal">
                <div class="video-wrap">
                    <?php if( have_rows('automation') ): ?>
                        <?php while( have_rows('automation') ): the_row(); ?>
                            <?php echo get_sub_field('video_frame'); ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="close-modal">
                    <svg class="tcb-icon" viewBox="0 0 320 512" data-id="icon-times-light" data-name="">
                        <path d="M193.94 256L296.5 153.44l21.15-21.15c3.12-3.12 3.12-8.19 0-11.31l-22.63-22.63c-3.12-3.12-8.19-3.12-11.31 0L160 222.06 36.29 98.34c-3.12-3.12-8.19-3.12-11.31 0L2.34 120.97c-3.12 3.12-3.12 8.19 0 11.31L126.06 256 2.34 379.71c-3.12 3.12-3.12 8.19 0 11.31l22.63 22.63c3.12 3.12 8.19 3.12 11.31 0L160 289.94 262.56 392.5l21.15 21.15c3.12 3.12 8.19 3.12 11.31 0l22.63-22.63c3.12-3.12 3.12-8.19 0-11.31L193.94 256z"></path>
                    </svg>
                </div>
            </div>
        </div>