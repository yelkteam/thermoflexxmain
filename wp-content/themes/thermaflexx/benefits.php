<?php 
/*
*
* Template name: Benefits
*
*/
get_header();?>
<div class="area" id="carousel">
    <?php the_post_thumbnail('full');?> 
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_wi-trans.png" class="divider top">
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_red-wi.png" class="divider bottom">
</div>
<div class="area" id="intro">
    <div class="grid">
      <div class="row">
        <div class="col14">
	        <?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>
	        <?php the_content();?>  
	        <?php endwhile;?>
	    	  <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<div class="area" id="products">
    <div class="grid">
      <div class="row">
        <div class="col20">

          <div class="full-list">
            <h2>ThermoFlexX <strong>benefits</strong></h2>
            <ul class="clearfix">
            	<?php if( have_rows('benefits_items') ): 
                while ( have_rows('benefits_items') ) : the_row();?>
                <li>
                  <div class="item">
                    <div class="photo">
                      <div class="inner"></div>
                      <img src="<?php the_sub_field('benefit_image');?>">
                    </div>
                    <div class="text">
                    	<h4><?php the_sub_field('benefit_title');?></h4>
	                    <?php the_sub_field('benefit_content');?>                    
					          </div>
                  </div>
                </li>
                <?php 
	                endwhile; 
	            endif;?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php get_footer();?>