jQuery(document).ready(function() {
  /* Header menu: mobilize */
  jQuery('header nav').meanmenu();

  /* Home carousel */
  jQuery('ul.home_carousel-off').bxSlider({
    mode: 'fade',
    speed: 500,
    auto: true,
    minSlides: 1,
    maxSlides: 1,
    slideMargin: 0,
    pause: 4000,
    moveSlides: 1,
    controls: false,
    pager: false,
    touchEnabled: false,
    swipeThreshold: 5,
    oneToOneTouch: false,
    tickerHover: true,
    adaptiveHeight: true
  });

  /* Detail carousel */
  jQuery('.slider').bxSlider({
    mode: 'vertical',
    speed: 500,
    auto: false,
    minSlides: 4,
    maxSlides: 4,
    slideMargin: 0,
    pause: 4000,
    moveSlides: 1,
    controls: true,
    pager: false,
    touchEnabled: true,
    swipeThreshold: 5,
    oneToOneTouch: true,
    tickerHover: true,
    adaptiveHeight: true,
    nextText: 'older',
    prevText: 'newer',
    infiniteLoop: false,
    hideControlOnEnd: true,
    onSlideAfter: function (currentSlideNumber, totalSlideQty, currentSlideHtmlObject) {
        jQuery('.selected').removeClass('selected');
        jQuery('.slider>li').eq(currentSlideHtmlObject).find('a').addClass('selected');
    },
    onSliderLoad: function () {
        jQuery('.slider>li:first-child a').addClass('selected');
    },
  });

  // Video overlay Btn
  jQuery(document).on("click", "[data-video]", function(e) {
    e.preventDefault();

    var video = jQuery(this).attr("data-video");

    jQuery("#video").show();
    jQuery(".item .btn-play").hide();
    jQuery("#video iframe").attr("src", video + "?rel=0&amp;controls=0showinfo=0&amp;autoplay=1");
  });

  jQuery(document).on("click", "#video .btn-close", function(e) {
    e.preventDefault();

    jQuery("#video").hide();
    jQuery("#video iframe").attr("src", "");
  });

  jQuery('.tablinks a').on('click', function(e){
    e.preventDefault();
  });

});

