<?php 
/*
*
* Template Name: Contacts
*
*/
get_header();?>
<div class="area" id="carousel">
    <?php the_post_thumbnail('full');?> 
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_wi-trans.png" class="divider top">
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_red-wi.png" class="divider bottom">
</div>
<div class="area" id="contact-hq">
    <div class="grid">
      <div class="row">
        <div class="col5">

          <div class="text-list">
            <h2>Contact</h2>
            <ul class="tab">
              <li class="tablinks" onclick="openCity(event, 'form')" id="defaultOpen"><a href="#">Contact us</a></li>
              <li class="tablinks" onclick="openCity(event, 'info')"><a href="#">Xeikon Corporate Offices &amp; Sales managers</a></li>
          	</ul>
          	<div class="social-links">
          		<?php if( have_rows('social_links') ): 
                	while ( have_rows('social_links') ) : the_row();?>
			          	<a href="<?php the_sub_field('social_link');?>" target="_blank" class="btn-social">
			          		<img src="<?php the_sub_field('social_image');?>" alt="social_link">
			            </a>
	            <?php 
	                endwhile; 
	            endif;?>
	        </div>
          </div>

        </div>
        <div class="col14 push1">

          <div class="vertical-line hq">

          	<div id="form" class="tabcontent">
              <h1><span>Xeikon Contact info</span></h1>
              <?php if (function_exists('tve_leads_form_display')) { tve_leads_form_display(0, 191); } ?>
			      </div>

      			<div id="info" class="tabcontent">
      				<?php the_field('contact_info'); ?>
      			</div>

          </div>

        </div>
      </div>
    </div>
</div>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<?php get_footer();?>