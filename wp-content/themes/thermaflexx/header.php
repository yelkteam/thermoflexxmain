<html style="" class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'favicon' ); } ?>">
    <title></title>

    <?php wp_head();?>
  </head>

  <body>
    <!--[if lt IE 7]>
      <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->


    <header class="area" id="header">
      <div class="grid">
        <div class="row">
          <div class="col20">
            <h1 class="logo"><a href="/" style="background:url('<?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'logo' ); } ?>') no-repeat left top; ">ThermoFlexX</a></h1>
            <nav class="main"> 
              <?php wp_nav_menu( array(
                'theme_location' => 'menu-top',
                'container'      => false
              ) ); ?>          
            </nav>
          </div>
        </div>
      </div>
    </header>