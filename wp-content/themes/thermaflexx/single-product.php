<?php get_header();?>
<div class="area" id="carousel">
 	<img src="<?php the_field('product_bg'); ?>" alt=""/>
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_wi-trans.png" class="divider top" alt="" />
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_red-wi.png" class="divider bottom" alt="" />
</div>
<div class="area" id="detail">
    <div class="grid">
      <div class="row">
        <div class="col5">

	        <div class="text-list">
	            <h2>Products</h2>
	            <ul>
	            	<?php wp_nav_menu( array(
		                'menu' => 'products'
		              ) ); ?>
				</ul>          
			</div>

	        <div class="question-form">
				<?php if (function_exists('tve_leads_form_display')) { tve_leads_form_display(0, 190); } ?>         
			</div>

	    </div>

        <div class="col14 push1">
          <div class="vertical-line">
            <div class="features">
              	<?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>
	        	<?php the_content();?>  
	        	<?php endwhile;?>
	    	  	<?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_wi-gr.jpg" class="divider" style="background: #f1f1f1;">
  <div class="area" id="technical">
    <div class="grid">
      <div class="row">
        <div class="col9">

     		<?php the_field('unique_vacuum_slider_concept'); ?>

        </div>
        <div class="col9 push2">

          <?php the_field('technical_characteristics'); ?>

        </div>
      </div>
      <div class="row">
        <div class="col9">

          <?php the_field('5080_dpi_exposure'); ?>

        </div>
        <div class="col9 push2">

          <div class="downloads">
            <h2>Download<br>product brochures</h2>
            <ul>
            <?php if( have_rows('download_product_brochures') ): 
                while ( have_rows('download_product_brochures') ) : the_row();?>
	            <li>
	                <a target="_blank" href="<?php the_sub_field('link');?>">
	                  <?php the_sub_field('title');?></a>
	            </li>
	        <?php 
                endwhile; 
            endif;?>
            </ul>
          </div>

        </div>
      </div>
    </div>
  </div>
<?php get_footer();?>