<?php get_header();?>
<div class="area" id="carousel">
    <?php echo get_the_post_thumbnail( 14, 'full' );?> 
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_wi-trans.png" class="divider top">
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_red-wi.png" class="divider bottom">
</div>
<div class="area" id="detail">
    <div class="grid">
      <div class="row">
        <div class="col5">
          <div class="text-list">
            <ul>
              <li><a href="/news">Back to All news &amp; events</a></li>
            </ul>
          </div>
        </div>
        <div class="col14 push1">
	        <div class="vertical-line">
	            <h1><?php the_title();?></h1>
	            <?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>
	            	<?php the_content();?>  
	            <?php endwhile;?>
            	<?php endif; ?>        
	        </div>
        </div>
      </div>
    </div>
</div>
<?php get_footer();?>