<?php 
/*
*
* Template Name: News
*
*/
get_header();?>
<div class="area" id="carousel">
    <?php the_post_thumbnail('full');?> 
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_wi-trans.png" class="divider top">
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_red-wi.png" class="divider bottom">
</div>
<div class="area" id="news-overview">
    <div class="grid">
      <div class="row">
        <div class="col5">
          &nbsp;
        </div>
        <div class="col14 push1">
          <div class="vertical-line overview">
            <ul>
            <?php
                $args = array(
                  'order' => 'DESC',
                  'posts_per_page'    =>  -1,
                );
                $query = new WP_Query( $args );
            ?>
            <?php if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post(); ?>
              <li>
                <h1><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
                <p><?php the_excerpt();?></p>
                <p class="description">Posted on <?php the_date('d F Y');?> -
                  <a href="<?php the_permalink();?>" class="more">read more</a>
              	</p>
              </li>
            <?php endwhile;?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php get_footer();?>