<?php 
/*
*
* Template name: Builder
*
*/
get_header();?>
<div class="area">
	<?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>
    	<?php the_content();?>  
    <?php endwhile;?>
	<?php endif; ?>
</div>
<?php get_footer();?>