<?php 
/*
*
* Template Name: Products
*
*/
get_header();?>
<div class="area" id="carousel">
    <?php the_post_thumbnail('full');?> 
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_wi-trans.png" class="divider top">
    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/divider_red-wi.png" class="divider bottom">
</div>
<div class="area" id="intro">
    <div class="grid">
      <div class="row">
        <div class="col14">
        	<?php if (have_posts() ) : while (have_posts() ) : the_post(); ?>
	        <?php the_content();?>  
	        <?php endwhile;?>
	    	  <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<div class="area" id="products">
    <div class="grid">
      <div class="row">
        <div class="col20">
          <div class="block-list">
            <h2>ThermoFlexX <b>for you</b></h2>
            <ul class="clearfix">
            	<?php
	                $args = array(
	                  'post_type' => 'product',
	                  'posts_per_page' =>  -1,
	                  'order' => 'ASC'
	                );
	                $query = new WP_Query( $args );
	            ?>
	            <?php if ($query -> have_posts() ) : while ($query -> have_posts() ) : $query -> the_post(); ?>
                <li>
                  <a href="<?php the_permalink();?>">
                    <div class="photo">
                      <div class="inner"></div>
                      <?php the_post_thumbnail(); ?>
                    </div>
                    <h4><?php the_title();?></h4>
                    <p><?php the_excerpt();?></p>
                  </a>
                </li>
              <?php endwhile;?>
	            <?php endif; ?>
	            <?php wp_reset_query(); ?>           
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php get_footer();?>