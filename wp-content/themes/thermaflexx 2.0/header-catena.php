<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="ie=edge" />
  <link rel="shortcut icon" type="image/x-icon" href="<?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'favicon' ); } ?>">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" />
  <title><?php bloginfo('title');?></title>
  <?php wp_head();?>
</head>

<body>
  <div id="wrapper">
    <header id="header">
      <div class="container">
        <div class="logo">
          <a href="/">
            <img src="<?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'logo' ); } ?>" alt="ThermoFlexx"/>
          </a>
        </div>
        <nav id="nav">
          <?php wp_nav_menu( array('menu'=>'Catena-W') ); ?>
        </nav>
        <div class="burger">
          <div class="wrapper">
            <span class="burger-line burger-line-first"></span>
            <span class="burger-line burger-line-second"></span>
            <span class="burger-line burger-line-third"></span>
            <span class="burger-line burger-line-fourth"></span>
          </div>
        </div>
      </div>
    </header>