<?php 
/*
*
* Template name: Download
*
*/
get_header();?>
	<main id="main" class="download">
		<img src="<?php the_post_thumbnail_url();?>" alt="" class="bg-img">
      <div class="container">
        <ul class="breadcrumbs">
          <li><a href="/">Home</a></li>
          <li class="disable">Download Area</li>
        </ul>
      </div>
      <div class="container">
        <h2 class="h1"><?php the_title();?></h2>
      </div>

      <!--section class="documentation-section">
        <div class="container">
          <div class="inner-wrapper">
            <h2>Documentation</h2>
            <div class="tabs">
              <ul class="tabs-list">
              	<?php $count = 0;
              		if( have_rows('products_menu') ): ?>
		            <?php while( have_rows('products_menu') ): the_row();
		            	$count++; ?>
                		<li class="<?php if($count == 1){echo 'active';}?>"><?php the_sub_field('title'); ?></li>
                	<?php endwhile; ?>
	  			<?php endif; wp_reset_postdata();?>
              </ul>
              <div class="tabs-wrapper">
              	<?php
              	$count = 0;
    	          $args = array(
      			      'post_type' => 'product',
      			      'posts_per_page' => -1,
      			      'order' => 'ASC'
      			    );
			         $query = new WP_Query( $args );?>
			         <?php if ( $query -> have_posts() ) : 
			    	    while ( $query -> have_posts() ) : $query -> the_post(); 
			    		     $count++; ?>
		                <div class="tabs-content <?php if($count == 1){echo 'active';}?>">
		                  <div class="img-block">
		                    <h3><?php the_field('category');?>
		                    	<span><?php the_field('short_title');?></span>
		                    </h3>
		                    <?php the_post_thumbnail();?>
		                  </div>
		                  <div class="descr-block">
		                    <h4>Product Sheet</h4>
		                    <ul class="download-list">
		                    	<?php if( have_rows('product_sheet') ): ?>
						            <?php while( have_rows('product_sheet') ): the_row();?>
		                      			<li><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></li>
		                      		<?php endwhile; ?>
	  							<?php endif; ?>
		                    </ul>
		                    <h4>Brochures</h4>
		                    <ul class="download-list">
		                    	<?php if( have_rows('brochures') ): ?>
						            <?php while( have_rows('brochures') ): the_row();?>
		                      			<li><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></li>
		                      		<?php endwhile; ?>
	  							<?php endif; ?>
		                    </ul>
		                  </div>
		                </div>
                	<?php 
                    endwhile; 
                    endif; 
                    wp_reset_query();
                  ?>
              </div>
            </div>
          </div>
        </div>
      </section-->
      <section class="documentation-section">
        <div class="container">
          <div class="inner-wrapper">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php the_content();?>
            <?php endwhile; ?>
            <?php endif; ?>
            <div class="tabs">
              <ul class="tabs-list">
                <?php $count = 0;
                  if( have_rows('products_menu') ): 
                  while( have_rows('products_menu') ): the_row();
                  $count++;
                ?>
                  <li class="<?php if($count == 1){echo 'active';}?>"><?php the_sub_field('title'); ?></li>
                <?php endwhile; ?>
                <?php endif; ?>
              </ul>
              <div class="tabs-wrapper">
                <?php
                  $tabs = 0;
                  if( have_rows('products_menu') ): 
                  while( have_rows('products_menu') ): the_row();
                  $tabs++;
                ?>
                  <?php 
                    if( have_rows('content') ): 
                    while( have_rows('content') ): the_row();
                  ?>
                  <div class="tabs-content <?php if($tabs == 1){echo 'active';}?>">
                    <div class="img-block">
                      <h3><?php the_sub_field('title'); ?></h3>
                      <?php $image = get_sub_field('image'); 
                        echo wp_get_attachment_image( $image, 'full' );
                      ?>
                    </div>
                    <div class="descr-block">
                      <?php if( have_rows('documents_block') ): ?>
                        <?php while( have_rows('documents_block') ): the_row(); ?>
                          <h4><?php the_sub_field('title'); ?></h4>
                          <ul class="download-list">
                            <?php if( have_rows('documents') ): ?>
                              <?php while( have_rows('documents') ): the_row(); ?>
                                <li><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></li>
                              <?php endwhile; ?>
                            <?php endif; ?>
                          </ul>
                        <?php endwhile; ?>
                      <?php endif; ?>
                    </div>
                  </div>
                  <?php endwhile; ?>
                  <?php endif; ?>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="guidelines-section">
        <div class="container">
          <div class="inner-wrapper">
            <div class="col">
              <h3>Brand Guidelines</h3>
              <ul class="download-list">
              	<?php if( have_rows('brand_guidelines') ): ?>
					<?php while( have_rows('brand_guidelines') ): the_row();?>
                		<li><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></li>
                	<?php endwhile; ?>
	  			<?php endif; ?>
              </ul>
            </div>
            <div class="col">
              <h3>White paper</h3>
              <ul class="download-list">
                <?php if( have_rows('white_paper') ): ?>
					<?php while( have_rows('white_paper') ): the_row();?>
                		<li><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></li>
                	<?php endwhile; ?>
	  			<?php endif; ?>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <?php get_template_part( 'contact-template' );?>
    </main>
<?php get_footer();?>