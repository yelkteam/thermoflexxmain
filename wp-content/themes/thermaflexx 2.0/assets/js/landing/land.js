(function($) {
	$('.bars').click(function(){
		$('.header_left_menu ul').toggleClass('active');
	});

	$( ".nav-item a" ).click(function(e) {
        e.preventDefault();
        if($('.header_left_menu ul').hasClass('active')){
			$('.header_left_menu ul').removeClass('active');
		}
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
    });

	$( ".up" ).click(function() {
    	$("html, body").animate({ scrollTop: 0 }, 500);
	});

	$(window).scroll(function() {
	    if ($(this).scrollTop() > 200) {
	        $(".up").stop().animate({
	            opacity: 1
	        }, 200);
	    } else {
	        $(".up").stop().animate({
	            opacity: 0
	        }, 200);
	    }

	    if ($(this).scrollTop() > 50) {
	    	$('.header_left_menu').addClass('sticky');
	    } else {
	    	$('.header_left_menu').removeClass('sticky');
	    }

	});

	$('.modal-call').click(function(){
		$('.overlay').addClass('active');
	});
	$('.close-modal').click(function(){
		$('.overlay').removeClass('active');
	});

})(jQuery);