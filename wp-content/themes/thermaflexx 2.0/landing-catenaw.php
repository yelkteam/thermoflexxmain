<?php 
/*
*
* Template Name: Catena-W
*
*/
get_header('catena');?>
<main id="main">
	<a href="#" class="btnUp"><i class="icon-arrow"></i></a>
	<section class="main-section">
		<div class="container">
			<h1 class="wow fadeInLeft">
				<?php the_field('title'); ?>
			</h1>
		</div>
		<div class="red-block"></div>
		<div class="img-block">
			<?php the_post_thumbnail('full',array('class'=>'wow fadeInUp','data-wow-duration'=>'1s'));?>
		</div>
	</section>
	<section id="features" class="features-section">
		<div class="container">
			<?php if( have_rows('features') ): while ( have_rows('features') ) : the_row();?>
			<h2><?php echo get_sub_field('features_title'); ?></h2>
			<div class="content-wrapper">
				<div class="col-left wow fadeInLeft">
					<?php 
          $items = get_sub_field('features_items');
          $count = count($items); 
          $half=round($count/2);
          foreach($items as $i => $item) { 
          if ($i == $half){?>
				</div>
				<div class="col-right wow fadeInRight">
					<?php } ?>
					<div class="item">
						<h3><?php echo $item['title']; ?></h3>
						<?php echo $item['description']; ?>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php endwhile;?>
			<?php endif;?>
		</div>
	</section>
	<section class="emission-section">
		<?php if( have_rows('slogan') ): while ( have_rows('slogan') ) : the_row();?>
		<div class="container">
			<p class="wow fadeInDown">
				<?=get_sub_field('slogan_title', false, false);?>
			</p>
		</div>
		<div class="img-wrapper">
			<?php 
          $slogan_image = get_sub_field('slogan_image');
          $slogan_image = wp_get_attachment_image( $slogan_image, 'full' );
          echo $slogan_image;
        ?>
		</div>
		<?php endwhile;?>
		<?php endif;?>
	</section>
	<section class="features-section with-img">
		<div class="container">
			<div class="content-wrapper">
				<?php if( have_rows('monitoring') ): while ( have_rows('monitoring') ) : the_row();?>
				<div class="col-left wow fadeInLeft">
					<div class="item">
						<?php
              $image = get_sub_field('image');
              $image = wp_get_attachment_image( $image, 'full' );
              echo $image;
              ?>
						<h3><?=get_sub_field('title');?></h3>
						<?=get_sub_field('description');?>
					</div>
				</div>
				<?php endwhile;?>
				<?php endif;?>
				<?php if( have_rows('pinbar') ): while ( have_rows('pinbar') ) : the_row();?>
				<div class="col-right wow fadeInRight">
					<div class="item">
						<?php
              $image = get_sub_field('image');
              $image = wp_get_attachment_image( $image, 'full' );
              echo $image;
              ?>
						<h3><?=get_sub_field('title');?></h3>
						<?=get_sub_field('description');?>
					</div>
				</div>
				<?php endwhile;?>
				<?php endif;?>
			</div>
		</div>
	</section>
	<section class="design-section">
		<div class="container">
			<?php if( have_rows('benefits') ): while ( have_rows('benefits') ) : the_row();?>
			<div class="col-left wow fadeInUp">
				<h3><?=get_sub_field('title');?></h3>
			</div>
			<div class="col-right wow fadeInDown">
				<?=get_sub_field('benefits');?>
			</div>
			<?php endwhile;?>
			<?php endif;?>
		</div>
	</section>
	<section class="calc-section">
		<div class="container">
			<div class="content-wrapper">
			<?php if( have_rows('calculator_block') ): while ( have_rows('calculator_block') ) : the_row();?>
				<div class="col col-left">
					<?=get_sub_field('calculator_text', false,false);?>
				</div>
				<div class="col col-right">
					<?php
						$image = get_sub_field('calculator_image');
              			$image = wp_get_attachment_image( $image, 'full' );
              			echo $image;
					?>
				</div>
			<?php endwhile;?>
			<?php endif;?>
			</div>
		</div>
	</section>
	<section id="spec" class="specifications-section">
		<div class="container">
			<?php if( have_rows('specifications') ): while ( have_rows('specifications') ) : the_row();?>
			<h2><?=get_sub_field('title');?></h2>
			<ul>
				<?php if( have_rows('specifications_items') ): while ( have_rows('specifications_items') ) : the_row();?>
				<li>
					<?=get_sub_field('icon');?>
					<h4><?=get_sub_field('title');?></h4>
					<p><?=get_sub_field('details');?></p>
				</li>
				<?php endwhile;?>
				<?php endif;?>
			</ul>
			<?php endwhile;?>
			<?php endif;?>
		</div>
	</section>
	<section id="contact" class="get-section">
		<div class="container">
			<?php 
        $cform = get_field('contact_form');
        if( $cform ): ?>
			<h2>
				<?=$cform['title'];?>
			</h2>
			<?php echo do_shortcode($cform['form']);?>
			<?php endif; ?>
		</div>
	</section>
</main>
<?php get_footer('catena');?>