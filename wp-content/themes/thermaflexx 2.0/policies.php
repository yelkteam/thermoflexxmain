<?php 
/*
*
* Template Name: Policies
*
*/
get_header();?>
	<main id="main" class="article policies">
		<section class="article-section">
			<div class="container">
          		<div class="inner-wrapper">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          				<?php the_content();?>
          			<?php endwhile; ?>
          			<?php endif; ?>
          		</div>
          	</div>
		</section>
	</main>
<?php get_footer();?>