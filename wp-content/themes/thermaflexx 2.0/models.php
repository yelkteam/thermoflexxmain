<?php 
/*
*
* Template name: Models
*
*/
get_header();?>
	<main id="main" class="categories">
	<img class="bg-img" src="<?php the_post_thumbnail_url();?>" alt="">
		<div class="container">
			<ul class="breadcrumbs">
				<li><a href="/">Home</a></li>
				<li class="disable">Products</li>
			</ul>
		</div>
		<section class="products-section">
			<div class="container">
				<div class="content-wrapper">
					<h1 class="h2">Products</h1>
					<ul class="product-list">
						<?php $models = array(
			               'taxonomy' => 'models'
			            );
			            $cats = get_categories($models);
			            foreach ($cats as $i => $cat):
			            ?>
						<li>
							<div class="title">
								<a class="category-link" href="#category-<?php echo $i; ?>">
									<h3><?php echo $cat->name;?></h3>
									<i class="icon-arrow-next"></i>
								</a>
							</div>
							<?php 
							$thumbnail = get_field('image', $cat->taxonomy . '_' . $cat->term_id);?>
							<img src="<?php echo $thumbnail;?>" alt="image description">
						</li>
						<?php endforeach;?>  
					</ul>
				</div>
			</div>
		</section>
		<?php
			$models = array(
	           'taxonomy' => 'models'
	        );
	        $cats = get_categories($models);
	        $arr_length = count($cats);
	        foreach ($cats as $i => $cat):
	    ?>
		<section id="category-<?php echo $i; ?>" class="product-list-section <?php if(++$i === $arr_length) { echo 'last'; }?>">
			<div class="container">
				<div class="title">
					<h3><?php echo $cat->name;?></h3>
					<p><?php echo $cat->description;?></p>
				</div>
				<div class="cards-wrapper">
					<?php
						$args = array(
							'post_type' => 'product',
							'posts_per_page' => '-1',
							'order' => 'ASC',
							'tax_query' => array(
								array(
									'taxonomy' => 'models',
									'field'    => 'slug',
									'terms'    => $cat->slug
								)
							)
						);
						$query = new WP_Query( $args );
					?>
					<?php if ( $query -> have_posts() ) : while ( $query ->  have_posts() ) : $query ->  the_post(); ?>
					<div class="card">
						<div class="content-wrapper">
								<?php the_post_thumbnail();?>
							<div class="description">
								<h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
								<p><?php the_excerpt();?></p>
								<a href="<?=get_the_permalink()?>">View Details <i class="icon-arrow-next"></i></a>
							</div>
							<ul class="details">
								<?php
									$value =  get_field('max_plate_size');
									if($value){
								?>
										<li>
											<span>Max. plate size:</span>
											<p><?php echo $value; ?></p>
										</li>
								<?php } ?>
								<?php
									$value =  get_field('standard_resolution');
									if($value){
								?>
										<li>
											<span>Standard Resolution:</span>
											<p><?php echo $value; ?></p>
										</li>
								<?php } ?>
							</ul>
						</div>
					</div>
					<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</section>
		<?php endforeach;?>
		<?php get_template_part( 'contact-template' );?>
	</main>
<?php get_footer();?>