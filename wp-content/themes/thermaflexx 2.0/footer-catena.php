    <footer id="footer">
      <div class="container">
        <div class="logo">
          <a href="/"><img src="<?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'logo' ); } ?>" alt="ThermoFlexx"/></a>
        </div>
        <nav>
          <ul>
            <?php if ( function_exists( 'ot_get_option' ) ) { 
              $social_links = ot_get_option( 'social_links', array() );
              if ( ! empty( $social_links ) ) {
                foreach( $social_links as $social_link ) {?>
                  <li>
                    <a target="_blank" href="<?php echo $social_link['href']; ?>">
                      <?php echo $social_link['title']; ?>
                    </a>
                  </li>
              <?php } 
              } 
            }?>
          </ul>
        </nav>
        <div class="copy">
          <?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'copyright' ); } ?>
        </div>
      </div>
    </footer>
  </div>
  <?php wp_footer();?>
</body>

</html>