<?php get_header();?>

  <main id="main" class="product">
      <section class="product-main-section" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/dist/images/single-bg.png');">
        <div class="container">
          <ul class="breadcrumbs">
            <li><a href="/">Home</a></li>
            <li><a href="/products">Products</a></li>
            <li class="disable"><?php the_title();?></li>
          </ul>
          <div class="inner-wrapper">
            <div class="col col-title">
              <h1 class="h1"><?php the_title();?></h1>
              <p class="descr"><?php echo get_the_excerpt();?></p>
            </div>
            <div class="col col-img">
              <?php the_post_thumbnail();?>
            </div>
          </div>
        </div>
      </section>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <section class="features-section">
        <div class="col-wrapper">
          <?php if(get_field('video_link')){?>
            <div class="video_wrapper video_wrapper_full js-videoWrapper">
              <iframe class="videoIframe js-videoIframe" frameborder="0" allowTransparency="true" allowfullscreen width="560" height="315" src="<?php the_field('video_link');?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              <button class="videoPoster js-videoPoster" style="background-image: url('<?php the_field('video_bg'); ?>');"></button>
            </div>
          <?php } else {?>
              <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/novideo.jpg" alt="" />
          <?php } ?>
        </div>
        <div class="container">
          <div class="inner-wrapper">
            <div class="content-wrapper">
              <h2>Features</h2>
              <ul class="features-list">
                <?php if( have_rows('custom_features') ): ?>
                  <?php while( have_rows('custom_features') ): the_row();?>

                    <li>
                      <div class="row">
                        <span><?php the_sub_field('title'); ?></span>
                        <strong><?php the_sub_field('description'); ?></strong>
                      </div>
                      <?php
                        $optional_title = get_sub_field('optional_title');
                        if($optional_title):
                      ?>
                      <div class="row">
                        <span><?php the_sub_field('optional_title'); ?></span>
                        <strong><?php the_sub_field('optional_description'); ?></strong>
                      </div>
                      <?php endif;?>
                    </li>

                  <?php endwhile; ?>
                <?php endif; ?>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <?php
        $calc_display = get_field('calc_show_block');
        if($calc_display){

          $description = get_field('calc_desc');
          $linktext = get_field('calc_link_text');
          $linkurl = get_field('calc_link_url');
          $linktarget = get_field('calc_link_target');
          $image = get_field('calc_img');
      ?>
        <section class="calc-section">
          <div class="container"> 
            <div class="content-wrapper">
              <div class="col col-left">
                <?=$description?>
                <?php
                  if($linktext){
                    $attr = '';
                    if($linktarget){
                      $attr = ' target="_blank"';
                    }
                ?>
                  <a href="<?=$linkurl?>"<?=$attr?> rel="noopener"><?=$linktext?><i class="icon-arrow"></i></a>
                <?php } ?>
              </div>
              <div class="col col-right">
                <?php
                  echo wp_get_attachment_image( $image, 'large' );
                ?>
              </div>
            </div>
          </div>
        </section>
      <?php } ?>
      <section class="brochures-section">
        <div class="container">
          <div class="inner-wrapper">
            <div class="col col-content">
              <?php if( have_rows('documents_block') ): ?>
              <?php while( have_rows('documents_block') ): the_row();?>
                <h2><?php the_sub_field('title');?></h2>
                <ul class="download-list">
                  <?php if( have_rows('documents') ): ?>
                  <?php while( have_rows('documents') ): the_row();?>
                    <li><a href="<?php the_sub_field('link'); ?>" target="_blank"><?php the_sub_field('title'); ?></a></li>
                  <?php endwhile; ?>
                  <?php endif; ?>
                </ul>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
            <div class="col col-img">
              <img src="<?php the_field('brochures_image');?>" alt="image description">
            </div>
          </div>
        </div>
      </section>
      <section class="action-section">
        <div class="container">
          <div class="inner-wrapper">
            <div class="col col-form">
              <div class="contact-block touch">
                <h3><?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'form_product_title' ); } ?></h3>
                <?php echo do_shortcode(get_field('contact_form_shortcode'));?>
              </div>
            </div>
            <div class="col col-content">
              <?php if( have_rows('technical_details') ): ?>
                    <?php while( have_rows('technical_details') ): the_row();?>
                    <h3><?php the_sub_field('title'); ?></h3>
                    <p><?php the_sub_field('description'); ?></p>
                  <?php endwhile; ?>
                <?php endif; ?>
            </div>
          </div>
        </div>
      </section>
      <?php get_template_part( 'contact-template' );?>

      <?php endwhile; ?>
      <?php endif; ?>
    </main>

<?php get_footer();?>