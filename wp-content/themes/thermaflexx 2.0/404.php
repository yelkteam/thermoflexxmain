<?php get_header();?>
<main id="main">
  <section class="not-found">
    <div class="container">
      <div class="inner-wrapper">
        <div class="content">
          <h2><span>4<strong>0</strong>4</span>Page Not Found</h2>
          <p>Sorry, but the page you are looking is not found.
            Please, make sure you have typed the current URL.</p>
          <a href="/" id="back" class="btn more">Go back to site</a>
        </div>
      </div>
    </div>
  </section>
</main>
<?php get_footer();?>