<?php get_header();?>
    <main id="main">
      <?php if( have_rows('first_screen') ): ?>
      <?php while( have_rows('first_screen') ): the_row(); 

        // Get sub field values.
        $slogan = get_sub_field('slogan');
        $benefits = get_sub_field('benefits');
        $background_image = get_sub_field('background_image');

        ?>
		  <section class="main-section">
		  	<picture class="bg-img-wrapper">
		  		<img class="bg-img" src="<?php echo $background_image; ?>" alt="">
			</picture>
            <div class="container">
              <div class="inner-wrapper">
                <h1><?php the_sub_field('title'); ?>

                </h1>
                <div class="slider-wrapper">

                  <div class="main-slider" id="mainSlider">
                    <?php if( have_rows('slider') ): ?>
                      <?php while( have_rows('slider') ): the_row();?>
                      <div class="item" data-title="<?php the_sub_field('slide_pagination'); ?>">
                        <div class="content-wrapper">
                          <a href="<?php the_sub_field('slide_link'); ?>">
                            <h2>
                              <span class="subtitle"><?php the_sub_field('category_title'); ?></span>
                              <?php the_sub_field('slide_title'); ?>
                            </h2>
                            <p><?php the_sub_field('slide_subtitle'); ?></p>
                            <span class="btn"><i class="icon-arrow-next"></i></span>
                          </a>
                        </div>
                        <?php
                          if( get_sub_field('slide_source') == 'video' ) {?>
                            <div class="overlay">
                    <video autoplay="autoplay" loop="loop" muted="muted" preload="auto" playsinline>
                              <source src="<?php the_sub_field('video_slide'); ?>" type="video/mp4">
                          </video>
                        </div>
                <?php } else if( get_sub_field('slide_source') == 'image' ) { ?>
                  <div class="overlay">
                            <img src="<?php echo the_sub_field('image_slide'); ?>" alt="image description">
                          </div>
                          <?php } ?>
                      </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </section>
      <?php endwhile; ?>
    <?php endif; ?>
    <?php if( have_rows('why_thermoflexx') ): ?>
      <?php while( have_rows('why_thermoflexx') ): the_row(); 

        // Get sub field values.
        $title = get_sub_field('title');
        $image = get_sub_field('image');
        $left_column = get_sub_field('left_column');
        $right_column = get_sub_field('right_column');
        ?>
        <section class="why-we-section">
          <div class="container">
            <div class="title-wrapper">
              <img src="<?php echo $image; ?>" alt="image description">
              <h2>
                <?php echo $title; ?>
              </h2>
            </div>
            <div class="content-wrapper">
              <div class="col">
                <?php echo $left_column; ?>
              </div>
              <div class="col">
                <?php echo $right_column; ?>
              </div>
              <div class="col col-fluid">
                <?php if( have_rows('bottom_column') ): ?>
            <?php while( have_rows('bottom_column') ): the_row(); 

              // Get sub field values.
              $title = get_sub_field('title');
              $subtitle = get_sub_field('subtitle');
              $image = get_sub_field('image');
              $details = get_sub_field('details');
              ?>
                  <div class="content">
                    <h2>
                      <span class="subtitle"><?php echo $subtitle; ?></span>
                      <?php echo $title; ?>
                    </h2>
                    <p><?php echo $details; ?></p>
                  </div>
                  <div class="img-block">
                    <img src="<?php echo $image; ?>" alt="TFX80 drum 01-1">
                  </div>
                <?php endwhile; ?>
            <?php endif; ?>
              </div>
            </div>
          </div>
        </section>
      <?php endwhile; ?>
    <?php endif; ?>
      <section class="miss-section">
        <div class="container">
          <div class="inner-wrapper">
            <div class="title-wrapper">
              <h2>
                <?php the_field('events_title');?>
              </h2>
            </div>
            <div class="slider-wrapper">
              <div class="miss-slider" id="miss-slider">
                <?php
              $args = array(
            'category_name' => 'events',
            'posts_per_page' => 3,
            'order' => 'DESC'
        );
        $wp_query = new WP_Query( $args );?>
        <?php if ( $wp_query -> have_posts() ) : while ( $wp_query -> have_posts() ) : $wp_query -> the_post(); ?>
                <div class="item">
                  <div class="content-wrapper">
                    <a href="<?php the_permalink();?>">
                      <p><?php the_title();?></p>
                    </a>
                  </div>
                  <div class="date-block">
                    <p class="time"><?php the_date('d M Y')?></p>
                    <address class="address"><?php the_field('location');?></address>
                  </div>
                </div>
                <?php 
                  endwhile; 
                  endif; 
                  wp_reset_query();
                ?>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="news-section">
        <div class="container">
          <div class="title-wrapper">
            <h2><?php the_field('news_title');?></h2>
            <a href="/news" class="btn more">All News</a>
          </div>
          <div class="slider-wrapper">
            <div class="news-slider" id="news-slider">
              <?php
              $args = array(
            'category_name' => 'news',
            'posts_per_page' => 3,
            'order' => 'DESC'
        );
        $wp_query = new WP_Query( $args );?>
        <?php if ( $wp_query -> have_posts() ) : while ( $wp_query -> have_posts() ) : $wp_query -> the_post(); ?>
                <div class="item">
                  <div class="content-wrapper">
                    <p class="time"><?php the_date('d F Y')?></p>
                    <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                    <a href="<?php the_permalink();?>" class="btn more">Read More</a>
                  </div>
                  <?php the_post_thumbnail();?>
                </div>
                <?php 
                  endwhile; 
                  endif; 
                  wp_reset_query();
                ?>
            </div>
          </div>
        </div>
        <div class="progress-wrapper">
          <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
      </section>
      <?php get_template_part( 'contact-template' );?>
    </main>
<?php get_footer();?>