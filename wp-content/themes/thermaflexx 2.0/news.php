<?php 
/*
*
* Template name: News
*
*/
get_header();?>
  <main id="main" class="news">
    <img src="<?php the_post_thumbnail_url();?>" alt="" class="bg-img">
      <div class="container">
        <ul class="breadcrumbs">
          <li><a href="/">Home</a></li>
          <li class="disable">News</li>
        </ul>
      </div>
      <div class="container">
        <h2 class="h1"><?php the_title();?></h2>
      </div>
      <section class="news-section">
        <div class="container">
          <div class="slider-wrapper">
            <div class="news-slider" id="news-slider">
            <?php
            $args = array(
				      'category_name' => 'news',
				      'posts_per_page' => 3,
				      'order' => 'DESC'
				    );
				    $wp_query = new WP_Query( $args );?>
				    <?php if ( $wp_query -> have_posts() ) : while ( $wp_query -> have_posts() ) : $wp_query -> the_post(); ?>
	              <div class="item">
	                <div class="content-wrapper">
	                  <p class="time"><?php echo get_the_date('d F Y');?></p>
	                  <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	                  <a href="<?php the_permalink();?>" class="btn more">Read More</a>
	                </div>
	                <?php the_post_thumbnail();?>
	              </div>
	            <?php endwhile; endif; wp_reset_query();?>
            </div>
          </div>
        </div>
        <div class="progress-wrapper">
          <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
      </section>
      <section class="news-list-section">
        <div class="container">
          <h2>Recent news</h2>
          <ul>
          	<?php
          	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          	$args = array(
          		'post_type' => 'post',
      			  'posts_per_page' => 9,
      			  'paged' => $paged
      			);
      			$query = new WP_Query( $args );?>
          	<?php if ( $query -> have_posts() ) : while ( $query -> have_posts() ) : $query -> the_post(); ?>
            <li>
              <div class="card-news">
                <picture>
                  <?php
                   if(has_post_thumbnail()){
                    the_post_thumbnail();
                   }else{
                    echo '<img src="/wp-content/uploads/2020/03/news-slide-1.png" alt=news/>';
                   }
                  ?>
                </picture>
                <div class="time"><?php echo get_the_date('d F Y');?></div>
                <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
              </div>
            </li>
            <?php endwhile; endif; wp_reset_query();?>
          </ul>
          <nav class="pagination">
            <?php previous_posts_link( '<i class="icon-arrow-prev"></i>' ); ?>
            <p>Page <span class="pagination__active"><?php echo $paged;?></span> of <span><?php echo $query->max_num_pages;?></span></p>
            <?php echo $nextLink = get_next_posts_link( '<i class="icon-arrow-next"></i>' , $query->max_num_pages ); ?>
          </nav>
        </div>
      </section>
      <?php get_template_part( 'contact-template' );?>
    </main>
<?php get_footer();?>