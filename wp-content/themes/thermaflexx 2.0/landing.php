<?php 
/*
*
* Template name: Landing
*
*/
get_header('land');?>

<div id="features" class="features wrapper">
	<h2><span>F</span>eatures</h2>
	<div class="features-wrapper">
		<?php if( have_rows('features') ): ?>
			<?php while( have_rows('features') ): the_row(); ?>
			    <div class="features-column">
			        <h3><?php echo get_sub_field('title'); ?></h3>
			        <p><?php echo get_sub_field('text'); ?></p>
			    </div>
	    	<?php endwhile; ?>
	    <?php endif; ?>
	</div>
</div>

<div class="about wrapper">
	<?php if( have_rows('auto-calibration') ): ?>
		<?php while( have_rows('auto-calibration') ): the_row(); ?>
			<div class="about-column">
				<h3><?php echo get_sub_field('title'); ?></h3>
				<div class="about-mobile-img">
					<img src="<?php echo get_sub_field('image'); ?>" alt=""/>
					<div class="gray-rectangle"></div>
				</div>
				<p><?php echo get_sub_field('text'); ?></p>	
			</div>
			<div class="about-column">
				<img class="wow fadeInUp" src="<?php echo get_sub_field('image'); ?>" alt=""/>
				<div class="gray-rectangle"></div>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>

<div class="news wrapper">
	<?php if( have_rows('news') ): ?>
		<?php while( have_rows('news') ): the_row(); ?>
			<div class="news-column wow fadeInUp">
				<img src="<?php echo get_sub_field('image'); ?>" alt="" />
				<h3><?php echo get_sub_field('title'); ?></h3>
				<p><?php echo get_sub_field('text'); ?></p>
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>

<div class="about-2">
	<?php if( have_rows('automation') ): ?>
		<?php while( have_rows('automation') ): the_row(); ?>
			<div class="about-2-content">
				<h3><?php echo get_sub_field('title'); ?></h3>
				<p><?php echo get_sub_field('text'); ?></p>
				<div class="play-group">
					<button class="modal-call">
						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/landing/play.svg" alt="" />
			    	</button>
			        <p><?php echo get_sub_field('video_text'); ?></p>
				</div>
			</div>
			<div class="img">
				<img class="wow fadeInRight" src="<?php echo get_sub_field('image'); ?>" alt="" />
			</div>
		<?php endwhile; ?>
	<?php endif; ?>
</div>

<div id="specifications" class="specifications wrapper">
	<h2><span>S</span>pecifications</h2>
	<div class="specifications-wrapper">
		<?php if( have_rows('specifications') ): ?>
			<?php while( have_rows('specifications') ): the_row(); ?>
				<div class="specifications-item">
					<img src="<?php echo get_sub_field('icon'); ?>" alt="" />
					<div class="specifications-item-details">
						<h3><?php echo get_sub_field('title'); ?></h3>
						<ul>
							<?php if( have_rows('details') ): ?>
								<?php while( have_rows('details') ): the_row(); ?>
									<li><?php echo get_sub_field('detail'); ?></li>
								<?php endwhile; ?>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>

<div id="contact" class="contact">
	<div class="contact-column">
		<h2><?php the_field('contact_us');?></h2>
	</div>
	<div class="contact-column">
		<?php echo do_shortcode('[thrive_leads id="537"]');?>
	</div>
</div>

<?php get_footer('land');?>