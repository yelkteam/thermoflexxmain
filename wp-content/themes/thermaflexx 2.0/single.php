<?php get_header();?>
  <main id="main" class="article">
    <div>
      <img src="<?php if ( function_exists( 'ot_get_option' ) ) { $test_input = ot_get_option( 'news_background' ); echo $test_input['background-image']; } ?>" alt="news-background"/>
    </div>
      <div class="container">
        <ul class="breadcrumbs">
          <li><a href="/">Home</a></li>
          <li><a href="/news">News & Events</a></li>
          <li class="disable"><?php the_title();?></li>
        </ul>
      </div>
      <section class="article-section">
        <div class="container">
          <div class="inner-wrapper">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <div class="title">
                <h2 class="h1"><?php the_title();?></h2>
                <div class="time"><?php the_date('d F Y');?></div>
              </div>
              
              <?php the_content();?>
            <?php endwhile; endif; ?>
            <div class="references">
              <?php
                $i=0;
                if( have_rows('references') ): ?>
                <h2>References:</h2>
                <ul>
                  <?php while( have_rows('references') ): the_row();
                    $i++;?>
                    <li>
                      <strong>[<?=$i;?>]</strong> <?php the_sub_field('reference'); ?>
                    </li>
                  <?php endwhile; ?>
                </ul>
              <?php endif; ?>
              <?php
                $text = get_the_title();
                $url = get_the_permalink();
                $description = get_the_excerpt();
                $logo = ot_get_option( 'logo' );
              ?>
              <div class="share">
                <h3>Share:</h3>
                <ul>
                  <li>
                    <a href="http://twitter.com/share?text=<?=$text ?>&url=<?=$url?>"  onclick="window.open(this.href, '', 'toolbar=0, status=0, width=548, height=325'); return false"><i class="icon-twitter"></i></a>
                  </li>
                  <li>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?=$url?>" onclick="window.open(this.href, '', 'toolbar=0, status=0, width=548, height=325'); return false"><i class="icon-social-facebook"></i></a>
                  </li>
                  <li>
                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=$url?>&title=<?=urlencode( $text)?>&source=LinkedIn" onclick="window.open(this.href, '', 'toolbar=0, status=0, width=548, height=325'); return false"><i class="icon-linkedin2"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="news-list-section">
        <div class="container">
          <div class="title-wrapper">
            <h2>More news & events</h2>
            <a href="/news" class="btn more">All news</a>
          </div>
          <ul>
            <?php
            $args = array(
              'category_name' => 'news',
              'posts_per_page' => 3,
              'order' => 'DESC',
              'post__not_in' => array( $post->ID )
            );
            $wp_query = new WP_Query( $args );?>
           <?php if ( $wp_query -> have_posts() ) : while ( $wp_query -> have_posts() ) : $wp_query -> the_post();?>
            <li>
              <div class="card-news">
                <picture>
                  <?php
                   if(has_post_thumbnail()){
                    the_post_thumbnail();
                   }else{
                    echo '<img src="/wp-content/uploads/2020/03/news-slide-1.png" alt=news/>';
                   }
                  ?>
                </picture>
                <div class="time"><?php the_date('d F Y')?></div>
                <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
              </div>
            </li>
            <?php 
            endwhile; 
            endif; 
            wp_reset_query();
            ?>
          </ul>
        </div>
      </section>
      <?php get_template_part( 'contact-template' );?>
    </main>
<?php get_footer();?>