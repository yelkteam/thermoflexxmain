<?php 
/*
*
* Template Name: Sitemap
*
*/
get_header();?>
    <main id="main">
    	<section class="main-section" style="background-image: url('<?php echo $background_image; ?>');">
            <div class="container">
              	<div class="inner-wrapper">
              		<?php the_title('<h1>','</h1>'); ?>
            		<ul class="sitemap-list">
            			<?php 
                		$categories = get_terms( 'category' );
                		if($categories){
                    		foreach ($categories as $category) {?>
                    			<li class="sitemap-list__item">
                                <a href="<?=get_category_link( $category->term_id)?>"><?=$category->name?></a>
                                <?php 
                        			$pages = get_posts(
	                                    array(
                                            'numberposts' => -1,
                                            'post_type' => 'post',
                                            'orderby' => 'title',
                                            'order' => 'ASC',
                                            'category' => $category->term_id,
	                                    )
                                    );
                        			if($pages){
                            	?>
                            		<ul class="sitemap-list--nested">
		                            <?php 
		                                foreach ($pages as $page) {?>
	                                        <li class="sitemap-list__item--nested">
	                                            <a href="<?=get_the_permalink($page->ID)?>"><?=$page->post_title?></a>
	                                        </li>
		                                <?php } ?>
		                            </ul>
                            	<?php } ?>
                    		<?php } ?>
                    	<?php } ?>

                    	<?php 
                		$tags = get_terms( 'post_tag' );
                		if($tags){
                    		foreach ($tags as $tag) {?>
                    			<li class="sitemap-list__item">
                    				<a href="<?=get_term_link( $tag->term_id)?>"><?=$tag->name?></a>
                    		<?php } ?>
                    	<?php } ?>

                    	<?php
		                $pages = get_posts(array('numberposts' => -1, 'post_type' => 'page','orderby' => 'title', 'order' => 'ASC','exclude' => array(get_the_ID()) ));
		                if($pages){?>
	                        <?php foreach ($pages as $page) { ?>
                                <li class="sitemap-list__item">
                                    <a href="<?=get_the_permalink($page->ID)?>"><?=$page->post_title?></a>
                                </li>
	                        <?php } ?>
		                <?php } ?>
            		</ul>
              	</div>
          	</div>
      	</section>
    </main>
<?php get_footer();?>