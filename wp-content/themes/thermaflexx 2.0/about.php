<?php 
/*
*
* Template name: About
*
*/
get_header();?>
  <main id="main" class="about-us">
    <img src="<?php the_post_thumbnail_url();?>" alt="" class="bg-img">
      <div class="container">
        <ul class="breadcrumbs">
          <li><a href="/">Home</a></li>
          <li class="disable">About us</li>
        </ul>
      </div>
      <div class="container">
        <h2 class="h1"><?php the_title();?></h2>
      </div>
      <section class="about-section reverse">
        <div class="container">
          <div class="inner-wrapper">
            <div class="col col-title">
              <h2><?php the_field('about_slogan')?><span><?php the_field('about_subslogan')?></span></h2>
              <img src="<?php the_field('about_image')?>" alt="image-description">
            </div>
            <div class="col col-description">
              <div class="content-wrapper">
              	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	                <?php the_content();?>
                  <?php endwhile; endif; wp_reset_postdata();?>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="about-section">
        <div class="container">
        	<?php if( have_rows('solutions') ): ?>
				<?php while( have_rows('solutions') ): the_row();?>
		          <div class="inner-wrapper">
		            <div class="col col-title">
		              <h2><span><?php the_sub_field('solution_subtitle')?></span><?php the_sub_field('solution_title')?></h2>
		              <img src="<?php the_sub_field('solution_image')?>" alt="image-description">
		            </div>
		            <div class="col col-description">
		              <div class="content-wrapper">
		              	<?php if( have_rows('solution_items') ): ?>
		              		<?php while( have_rows('solution_items') ): the_row();?>
				                <h3><?php the_sub_field('title')?></h3>
				                <p><?php the_sub_field('text')?></p>
				            <?php endwhile; ?>
	  					<?php endif; ?>
		              </div>
		            </div>
            	<?php endwhile; ?>
	  		<?php endif; ?>
          </div>
        </div>
      </section>
      <section class="about-section fluid">
        <div class="container">
          <div class="col col-title">
            <h2><?php the_field('benefit_title')?><span><?php the_field('benefit_subtitle')?></span></h2>
            <img src="<?php the_field('benefit_image')?>" alt="image-description">
          </div>
          <div class="col col-description">
            <ul>
            	<?php if( have_rows('benefits_items') ): ?>
					<?php while( have_rows('benefits_items') ): the_row();?>
		              <li>
		                <h3><?php the_sub_field('title')?> </h3>
		                <?php the_sub_field('content')?>
		              </li>
	              	<?php endwhile; ?>
		  		<?php endif; ?>
            </ul>
          </div>
        </div>
      </section>
      <?php get_template_part( 'contact-template' );?>
    </main>
<?php get_footer();?>