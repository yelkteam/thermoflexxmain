<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'google_analytics' ); } ?>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="shortcut icon" media="print" type="image/x-icon" href="<?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'favicon' ); } ?>">
  <!-- Google Fonts -->
  <!--link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet"-->
  <title><?php bloginfo('title');?></title>
  <style>
@font-face {
  font-family: 'Open Sans';
  src: url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-Bold.woff2') format('woff2'), url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-Bold.woff') format('woff');
  font-weight: bold;
  font-style: normal;
  font-display: block;
}
@font-face {
  font-family: 'Open Sans';
  src: url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-Light.woff2') format('woff2'), url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-Light.woff') format('woff');
  font-weight: 300;
  font-style: normal;
  font-display: block;
}
@font-face {
  font-family: 'Open Sans';
  src: url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-Regular.woff2') format('woff2'), url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-Regular.woff') format('woff');
  font-weight: normal;
  font-style: normal;
  font-display: block;
}
@font-face {
  font-family: 'Open Sans';
  src: url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-SemiBold.woff2') format('woff2'), url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-SemiBold.woff') format('woff');
  font-weight: 600;
  font-style: normal;
  font-display: block;
}
@font-face {
  font-family: 'Open Sans';
  src: url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-ExtraBold.woff2') format('woff2'), url('<?php echo get_stylesheet_directory_uri();?>/dist/fonts/OpenSans-ExtraBold.woff') format('woff');
  font-weight: 800;
  font-style: normal;
  font-display: block;
}
</style>
  <?php wp_head();?>
</head>

<body>
  <div id="wrapper">
  <div class="block-submenu hidden">
        <div class="col col-first">
          <picture>
            <img src="<?php echo get_stylesheet_directory_uri();?>/dist/images/img-subtitle-img.jpg" alt="" />
          </picture>
        </div>

        <?php
            $tax_name = 'models';
            $productCats = get_terms( [
              'taxonomy' => $tax_name,
              //'number' => 3,
            ] );
            if($productCats){
              foreach($productCats as $cat){
                
                $catPosts = get_posts(array(
                                            'post_type' => 'product',
                                            'tax_query' => array(
                                              array(
                                                'taxonomy' => $tax_name,
                                                'field' => 'id',
                                                'terms' => $cat->term_id,
                                              )
                                            )
                                      ));
                ?>
                <div class="col col-hasmenu">
                  <?php foreach($catPosts as $ind => $item){ ?>
                    <div class="img-wrap" data-index="<?=$ind?>">
                      <?=get_the_post_thumbnail($item->ID,'medium')?>
                    </div>
                  <?php } ?>
                  <div class="wrapper">
                    <div class="content">
                      <span class="category-name"><?=$cat->name?></span>
                      <ul class="products-list">
                        <?php foreach($catPosts as $ind => $item){ ?>
                          <li>
                            <a data-id="<?=$ind?>" href="<?=get_the_permalink($item->ID)?>"><?=$item->post_title?> <i class="icon-arrow-next"></i></a>
                          </li>
                        <?php } ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <?php
              }
            }
        ?>

        <div class="menu-footer">
          <h2>Product</h2>
          <a href="<?=get_the_permalink(10)?>">View all <i class="icon-arrow-next"></i></a>
        </div>
      </div>
    <header id="header">
      <div class="container">
        <div class="inner-wrapper">
          <div class="logo-wrapper">
            <a href="/">
              <img src="<?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'logo' ); } ?>" alt="ThermoFlexx">
            </a>
          </div>
          <nav id="nav">
            <?php wp_nav_menu( array(
                'theme_location' => 'menu-top',
                'container'      => false
              ) ); ?>
          </nav>
          <div class="burger">
            <div class="burger-brick"></div>
            <div class="burger-brick middle"></div>
            <div class="burger-brick"></div>
          </div>
        </div>
      </div>
    </header>