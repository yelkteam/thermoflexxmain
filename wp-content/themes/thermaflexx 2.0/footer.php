    <footer id="footer">
      <div class="container">
        <div class="inner-wrapper">
          <div class="copyright">
            <?php if ( function_exists( 'ot_get_option' ) ) { echo $test_input = ot_get_option( 'copyright' ); } ?>
          </div>
          <nav>
            <?php wp_nav_menu( array(
                'theme_location' => 'menu-bottom',
                'container'      => false
              ) ); ?>
          </nav>
        </div>
      </div>
    </footer>
  </div>
  <?php wp_footer();?>
</body>

</html>