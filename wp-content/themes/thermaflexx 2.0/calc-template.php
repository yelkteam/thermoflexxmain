<?php
/*
* Template Name: Calc
*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset=utf-8>
		<meta http-equiv=X-UA-Compatible content="IE=edge">
		<meta name=viewport content="width=device-width,initial-scale=1">
		<link rel=icon href="/wp-content/uploads/2020/01/favicon.png">
		<title><?=the_title();?></title>
		<link href=/wp-content/themes/thermaflexx%202.0/assets/js/calc/chunk-2d0bd94a.js rel=prefetch>
      	<link href=/wp-content/themes/thermaflexx%202.0/assets/js/calc/chunk-2d0c9193.js rel=prefetch>
      	<link href=/wp-content/themes/thermaflexx%202.0/assets/js/calc/chunk-2d0cfe51.js rel=prefetch>
      	<link href=/wp-content/themes/thermaflexx%202.0/assets/js/calc/chunk-2d0d2fac.js rel=prefetch>
      	<link href=/wp-content/themes/thermaflexx%202.0/assets/js/calc/chunk-2d21731b.js rel=prefetch>
      	<link href=/wp-content/themes/thermaflexx%202.0/assets/js/calc/chunk-2d2297db.js rel=prefetch>
      	<link href=/wp-content/themes/thermaflexx%202.0/assets/css/calc/app.css rel=preload as=style>
      	<link href=/wp-content/themes/thermaflexx%202.0/assets/js/calc/app.js rel=preload as=script>
      	<link href=/wp-content/themes/thermaflexx%202.0/assets/js/calc/chunk-vendors.js rel=preload as=script>
      	<link href=/wp-content/themes/thermaflexx%202.0/assets/css/calc/app.css rel=stylesheet>
	</head>
	<body>
		<noscript>
			<strong>We're sorry but catena_calculator doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
		</noscript>
		<div id=app></div>
		<script src=/wp-content/themes/thermaflexx%202.0/assets/js/calc/chunk-vendors.js></script>
      	<script src=/wp-content/themes/thermaflexx%202.0/assets/js/calc/app.js></script>
		<script type="text/javascript">
	    	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	    </script>
	</body>
</html>
