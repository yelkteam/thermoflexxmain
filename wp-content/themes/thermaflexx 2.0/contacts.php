<?php 
/*
*
* Template name: Contacts
*
*/
get_header();?>
<main id="main" class="contact">
	<img src="<?php the_post_thumbnail_url();?>" alt="" class="bg-img">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/">Home</a></li>
            <li class="disable">Contact us</li>
        </ul>
    </div>
    <div class="container">
        <h2 class="h1"><?php the_title();?></h2>
    </div>
    <section class="contact-us-section">
        <div class="container">
            <div class="inner-wrapper">
            	<?php if( have_rows('main_location') ): ?>
	                <?php while( have_rows('main_location') ): the_row();
	                	$phone = get_sub_field('phone');
	                	$phone_number = str_replace(array('(', ')', ' ', '-'), '', $phone);
	                	?>
		                <div class="contact-item">
		                    <div class="col col-title">
		                        <h3><?php the_sub_field('title');?></h3>
		                    </div>
		                    <div class="col col-content">
		                        <div class="contact-block">
		                            <address>
		                                <p><?php the_sub_field('location_title');?></p>
		                                <ul>
		                                    <li><span class="location"><?php the_sub_field('location');?></span></li>
		                                    <li><a class="tel" href="tel:<?php echo $phone_number; ?>" ><?php the_sub_field('phone');?></a></li>
		                                </ul>
		                            </address>
		                        </div>
		                    </div>
		                </div>
	                <?php endwhile; ?>
                <?php endif; ?>
                <div class="contact-item">
                	<?php if( have_rows('sales_managers') ): ?>
	                  	<?php while( have_rows('sales_managers') ): the_row();?>
		                    <div class="col col-title">
		                        <h3><?php the_sub_field('title');?></h3>
		                    </div>
		                    <div class="col col-content tabs">
		                        <ul class="tabs-list">
		                        	<?php $count = 0; 
		                        		if( have_rows('regions') ): ?>
	                  					<?php while( have_rows('regions') ): the_row();
	                  						$count++;
	                  						?>
		                            		<li class="<?php if($count == 1){echo 'active';}?>"><?php echo get_sub_field('title');?></li>
				                        <?php endwhile; ?>
		                			<?php endif; ?>
		                        </ul>
		                        <?php $count = 0; 
	                        		if( have_rows('regions') ): ?>
                  					<?php while( have_rows('regions') ): the_row();
                  						$count++;
                  						?>
				                        <div class="tabs-content <?php if($count == 1){echo 'active';}?>">
				                        	<?php if( have_rows('offices') ): ?>
                  								<?php while( have_rows('offices') ): the_row();?>
                  									<?php
                  										$phone = get_sub_field('phone');
                  										$phone_number = str_replace(array('(', ')', ' ', '-'), '', $phone);

                  										$location_title = get_sub_field('location_title');
                  										$email = get_sub_field('email');
                  										$full_name = get_sub_field('full_name');
                  									?>
						                            <div class="contact-block">
						                                <address>
						                                    <p><?php echo get_sub_field('title');?></p>
						                                    <ul>
						                                    	<?php if($location_title){?>
						                                    		<li><span class="location"><?php echo $location_title;?></span></li>
						                                    	<?php } ?>
						                                    	<?php if($phone){?>
						                                        	<li><a class="tel" href="tel:<?php echo $phone_number;?>"><?php echo $phone;?></a></li>
						                                        <?php } ?>
						                                        <?php if($email){?>
						                                        	<li><a href="mailto:<?php echo $email;?>" class="mail"><?php echo $email;?></a></li>
						                                        <?php } ?>
						                                        <?php if($full_name){?>
						                                        	<li><span class="user"><?php echo $full_name;?></span></li>
						                                        <?php } ?>
						                                    </ul> 
						                                </address>
						                            </div>
												<?php endwhile; ?>
		                					<?php endif; ?>
				                        </div>
		                        	<?php endwhile; ?>
		                		<?php endif; ?>
                    		</div>
                   		<?php endwhile; ?>
                	<?php endif; ?>
                </div>
                <div class="contact-item">
                	<?php if( have_rows('support_contacts') ): ?>
	                  	<?php while( have_rows('support_contacts') ): the_row();?>
		                    <div class="col col-title">
		                        <h3><?php the_sub_field('title');?></h3>
		                    </div>
		                    <div class="col col-content tabs">
		                        <ul class="tabs-list">
		                        	<?php $count = 0; 
	                        		if( have_rows('regions') ): ?>
                  						<?php while( have_rows('regions') ): the_row();
                  						$count++;
                  						?>
		                            		<li class="<?php if($count == 1){echo 'active';}?>"><?php echo get_sub_field('title');?></li>
		                            	<?php endwhile; ?>
		                			<?php endif; ?>
		                        </ul>
		                        <?php $count = 0; 
	                        		if( have_rows('regions') ): ?>
                  					<?php while( have_rows('regions') ): the_row();
                  						$count++;
                  						$title = get_sub_field('title');
                  						?>
				                        <div class="tabs-content <?php if($count == 1){echo 'active';}?>">
				                        	<?php if( have_rows('offices') ): ?>
                  								<?php while( have_rows('offices') ): the_row();
                  									$email = get_sub_field('email');
                  									?>
						                            <div class="contact-block">
						                                <address>
						                                    <p><?php echo $title;?></p>
						                                    <ul>
						                                    	<?php if($email){?>
						                                        	<li><a href="mailto:<?php echo $email;?>" class="mail"><?php echo $email;?></a></li>
						                                        <?php } ?>
						                                    </ul> 
						                                </address>
						                            </div>
				                            	<?php endwhile; ?>
		                					<?php endif; ?>
				                        </div>
		                        	<?php endwhile; ?>
		                		<?php endif; ?>
		                    </div>
	                    <?php endwhile; ?>
                	<?php endif; ?>
                </div>
                <div class="contact-item">
                    <div class="col col-title">
                        <h3><?php the_field('contact_form_title');?></h3>
                        <p class="descr"><?php the_field('contact_form_subtitle');?></p>
                    </div>
                    <div class="col col-content">
                        <div class="contact-block touch">
                            <?php echo do_shortcode(get_field('contact_form_shortcode'));?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php get_footer();?>