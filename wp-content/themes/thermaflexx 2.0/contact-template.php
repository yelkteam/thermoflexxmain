<section class="follow-section">
  <div class="container">
    <div class="inner-wrapper">
      <div class="col">
        <h3>Subscribe to Newsletter</h3>
        <?php echo do_shortcode("[thrive_leads id='671']");?>
      </div>
      <div class="col">
        <h3>Follow us:</h3>
        <ul>
          <?php if ( function_exists( 'ot_get_option' ) ) { 
            $social_links = ot_get_option( 'social_links', array() );
            if ( ! empty( $social_links ) ) {
              foreach( $social_links as $social_link ) {?>
                <li>
                  <a target="_blank" href="<?php echo $social_link['href']; ?>">
                    <?php echo $social_link['title']; ?>
                    
                  </a>
                </li>
            <?php } 
          } 
      }?>
        </ul>
      </div>
    </div>
  </div>
</section>