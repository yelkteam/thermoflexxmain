<?php 
/*
*
* Template Name: Catena-DL
*
*/
get_header('catena');?>
<main id="main">
  <a href="#" class="btnUp"><i class="icon-arrow"></i></a>
  <section class="main-section">
    <div class="container">
      <h1 class="wow fadeInLeft">
        <?php the_field('title'); ?>
      </h1>
    </div>
    <div class="red-block"></div>
    <div class="img-block">
      <?php the_post_thumbnail('full',array('class'=>'wow fadeInUp','data-wow-duration'=>'1s'));?>
    </div>
  </section>
  <section id="features" class="features-section">
    <div class="container">
      <?php if( have_rows('features') ): while ( have_rows('features') ) : the_row();?>
      <h2><?php echo get_sub_field('title'); ?></h2>
      <div class="block-list">
      	<?php 
      		$items = get_sub_field('features_items');
      		foreach($items as $i => $item):
      	?>
        <div class="item wow fadeInUp">
          <span>0<?=$i+1;?></span>
          <h3><?php echo $item['title']; ?></h3>
          <?php echo $item['description']; ?>
        </div>
    	<?php endforeach;?>
      </div>
      <?php endwhile;?>
      <?php endif;?>
      <div class="content-wrapper">
      	<?php if( have_rows('about') ): while ( have_rows('about') ) : the_row();?>
        <div class="item">
          <div class="block-img wow fadeInUp">
          	<?php
          		$image = get_sub_field('image');
          		$image = wp_get_attachment_image( $image, 'full' );
          		echo $image;
          	?>
          </div>
          <div class="block-content wow fadeInDown">
            <div class="wrapper">
              <h3><?php echo get_sub_field('title'); ?></h3>
              <?php echo get_sub_field('description'); ?>
            </div>
          </div>
        </div>
        <?php endwhile;?>
      	<?php endif;?>
      </div>
    </div>
  </section>
  <section id="spec" class="specifications-section">
    <div class="container">
      <?php if( have_rows('specifications') ): while ( have_rows('specifications') ) : the_row();?>
      <h2><?php echo get_sub_field('title'); ?></h2>
      <ul>
      	<?php
      		$items = get_sub_field('specifications_items');
      		foreach($items as $i => $item):
      	?>
        <li>
          <?php echo $item['icon'];?>
          <h4><?php echo $item['title'];?></h4>
          <p><?php echo $item['description'];?></p>
        </li>
        <?php endforeach;?>
      </ul>
      <?php endwhile;?>
      <?php endif;?>
    </div>
  </section>
  <section id="contact" class="get-section">
    <div class="container">
      <?php 
      $cform = get_field('contact_form');
      if( $cform ): ?>
	    <h2><?=$cform['title'];?></h2>
	    <?php echo do_shortcode($cform['form']);?>
      <?php endif; ?>
    </div>
  </section>
</main>
<?php get_footer('catena');?>